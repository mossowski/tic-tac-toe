<?php

namespace App\Providers;

use App\TicTacToe\Domain\Services\TicTacToeCacheDecorator;
use App\TicTacToe\Domain\TicTacToeInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TicTacToeInterface::class,TicTacToeCacheDecorator::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
