<?php


namespace App\TicTacToe\Responders;


interface ResponderInterface
{
    public function respond($data);
}
