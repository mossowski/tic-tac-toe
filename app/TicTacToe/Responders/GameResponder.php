<?php

namespace App\TicTacToe\Responders;

use App\TicTacToe\Domain\Models\Game;

class GameResponder implements ResponderInterface
{
    /**
     * @param Game $data
     * @return array
     */
    public function respond($data): array
    {
        return [
            "board" => $data->getBoardData(),
            "currentTurn" => $data->getCurrentTurn(),
            "score" => $data->getGameResults(),
            "victory" => $data->getWinner() ?? ""
        ];
    }
}
