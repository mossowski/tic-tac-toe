<?php

namespace App\TicTacToe\Responders;

use App\TicTacToe\Domain\Models\Game;

class DeleteGameResponder implements ResponderInterface
{
    /**
     * @param Game $data
     * @return array
     */
    public function respond($data): array
    {
        return [
            "currentTurn" => $data->getCurrentTurn()
        ];
    }
}
