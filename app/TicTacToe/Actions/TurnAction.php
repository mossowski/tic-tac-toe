<?php

namespace App\TicTacToe\Actions;

use App\TicTacToe\Domain\TicTacToeInterface;
use App\TicTacToe\Responders\ResponderInterface;
use App\TicTacToe\Responders\GameResponder;
use Illuminate\Http\Request;

class TurnAction
{
    private TicTacToeInterface $game;
    private ResponderInterface $responder;

    /**
     * StartAction constructor.
    */
    public function __construct(TicTacToeInterface $game, GameResponder $responder)
    {
        $this->game = $game;
        $this->responder = $responder;
    }

    public function __invoke(Request $request, $piece = ""): array
    {
        $game = $this->game->turn(
            $piece,
            $request->get("x"),
            $request->get("y")
        );
        return $this->responder->respond($game);
    }
}
