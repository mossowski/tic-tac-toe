<?php

namespace App\TicTacToe\Actions;

use App\TicTacToe\Domain\TicTacToeInterface;
use App\TicTacToe\Responders\DeleteGameResponder;
use App\TicTacToe\Responders\ResponderInterface;
use Illuminate\Http\Request;

class DeleteAction
{
    private TicTacToeInterface $game;
    private ResponderInterface $responder;

    /**
     * StartAction constructor.
    */
    public function __construct(TicTacToeInterface $game, DeleteGameResponder $responder)
    {
        $this->game = $game;
        $this->responder = $responder;
    }

    public function __invoke(Request $request): array
    {
        $game = $this->game->delete();
        return $this->responder->respond($game);
    }
}
