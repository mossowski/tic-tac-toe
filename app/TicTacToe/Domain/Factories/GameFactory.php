<?php

namespace App\TicTacToe\Domain\Factories;

use App\TicTacToe\Domain\Models\Game;
use App\TicTacToe\Domain\Models\Board;
use App\TicTacToe\Domain\Models\Player;
use App\TicTacToe\Domain\Models\Players;

class GameFactory
{
    public static function create(): Game
    {
        return new Game(
            new Board(),
            new Players(new Player("x"), new Player("o"))
        );
    }

    public function loadFromString(String $string): Game
    {
        return unserialize($string);
    }
}
