<?php

namespace App\TicTacToe\Domain\Services;

use App\TicTacToe\Domain\Models\Game;
use App\TicTacToe\Domain\Exceptions\DomainServiceException;
use App\TicTacToe\Domain\TicTacToeInterface;
use Psr\SimpleCache\{CacheInterface, InvalidArgumentException};
use Exception;

class TicTacToeCacheDecorator implements TicTacToeInterface
{
    private TicTacToeInterface $object;
    private CacheInterface $cache;
    public const CACHE_KEY = "tic_tac_toe_cache11";

    public function __construct(TicTacToe $object, CacheInterface $cache)
    {
        $this->object = $object;
        $this->cache = $cache;
    }

    /**
     * @throws DomainServiceException
     * @throws InvalidArgumentException
     */
    public function start(): Game
    {
        $this->load();
        $game = $this->object->start();
        return $this->save($game);
    }

    /**
     * @throws DomainServiceException|InvalidArgumentException
     */
    public function load(?Game $game = null): void
    {
        try {
            $game = $this->cache->get(self::CACHE_KEY);
            $this->object->load($game ? unserialize($game) : null);
        } catch (Exception $exception) {
            throw new DomainServiceException(
                "Can't load game state from cache",
                0,
                $exception
            );
        }
    }

    /**
     * @throws DomainServiceException|InvalidArgumentException
     */
    public function restart(): Game
    {
        $this->load();
        $game = $this->object->restart();
        return $this->save($game);
    }

    /**
     * @throws DomainServiceException|InvalidArgumentException
     */
    public function turn(string $playerName, int $col, int $row): Game
    {
        $this->load();
        $game = $this->object->turn($playerName, $col, $row);
        return $this->save($game);
    }

    /**
     * @throws DomainServiceException|InvalidArgumentException
     */
    public function delete(): Game
    {
        $this->load();
        $game = $this->object->delete();
        return $this->save($game);
    }


    /**
     * @throws DomainServiceException
     */
    private function save(Game $game): Game|null
    {
        try {
            $this->cache->set(self::CACHE_KEY, serialize($game));
            return $game;
        } catch (InvalidArgumentException $exception) {
            throw new DomainServiceException(
                "Can't set game state to cache",
                0,
                $exception
            );
        }
    }
}
