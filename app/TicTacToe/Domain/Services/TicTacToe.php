<?php

namespace App\TicTacToe\Domain\Services;

use App\TicTacToe\Domain\Exceptions\DomainServiceException;
use App\TicTacToe\Domain\Models\Game;
use App\TicTacToe\Domain\Factories\GameFactory;
use App\TicTacToe\Domain\TicTacToeInterface;

class TicTacToe implements TicTacToeInterface
{
    private ?Game $game = null;

    public function start(): Game
    {
        $this->game = $this->game ?? GameFactory::create();
        return $this->game;
    }

    public function load(?Game $game = null): void
    {
        if (!$this->game && $game) {
            $this->game = $game;
        }
    }

    /**
     * @throws DomainServiceException
     */
    public function restart(): Game
    {
        if (!$this->game) {
            throw new DomainServiceException("Can't process action - game is not started");
        }
        $this->game->restart();
        return $this->game;
    }

    /**
     * @throws DomainServiceException|\App\TicTacToe\Domain\Exceptions\DomainException
     */
    public function turn(string $playerName, int $col, int $row): Game
    {
        if (!$this->game) {
            throw new DomainServiceException("Can't process action - game is not started");
        }

        $this->game->turn($playerName, $col, $row);
        return $this->game;
    }

    /**
     * @throws DomainServiceException
     */
    public function delete(): Game
    {
        if (!$this->game) {
            throw new DomainServiceException("Can't process action - game is not started");
        }

        $this->game = GameFactory::create();
        return $this->game;
    }
}
