<?php

namespace App\TicTacToe\Domain;

use App\TicTacToe\Domain\Models\Game;

interface TicTacToeInterface
{
    public function start(): Game;
    public function load(?Game $game = null): void;
    public function restart(): Game;
    public function turn(string $playerName, int $col, int $row): Game;
    public function delete(): Game;
}
