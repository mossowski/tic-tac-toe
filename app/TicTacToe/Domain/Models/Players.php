<?php

namespace App\TicTacToe\Domain\Models;

class Players
{
    private array $collection;

    public function __construct(Player ...$players)
    {
        $this->collection = $players;
    }

    public function getPlayers(): array
    {
        return $this->collection;
    }

    public function getRandomPlayer(): Player
    {
        return $this->collection[array_rand($this->collection)];
    }

    public function getPlayerByName(string $name) :? Player
    {
        $result = array_filter($this->collection, function (Player $player) use ($name) {
           return $player->getName() === $name;
        });

        return $result ? reset($result) : null;
    }

    public function switchPlayer(Player $currentPlayer): Player
    {
        $result = array_filter($this->collection, function (Player $player) use ($currentPlayer) {
            return $player !== $currentPlayer;
        });

        return reset($result);
    }
}
