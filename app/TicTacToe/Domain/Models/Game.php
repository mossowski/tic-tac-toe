<?php

namespace App\TicTacToe\Domain\Models;

use App\TicTacToe\Domain\Exceptions\DomainException;
use App\TicTacToe\Domain\Exceptions\DomainNotAcceptableException;

class Game
{
    private Board $board;
    private Score $score;
    private Players $players;
    private Player $currentPlayer;
    private ?Player $winner = null;

    public function __construct(
        Board $board,
        Players $players
    ) {
        $this->board = $board;
        $this->score = new Score($players);
        $this->players = $players;
        $this->currentPlayer = $this->players->getRandomPlayer();
    }

    public function getBoardData(): array
    {
        return $this->board->getData();
    }

    public function restart(): void
    {
        if ($this->winner) {
            $this->score->addWinner($this->winner);
            $this->winner = null;
        }

        $this->board = new Board();
        $this->currentPlayer = $this->players->getRandomPlayer();
    }

    public function delete(): void
    {
        $this->score = new Score($this->players);
        $this->board = new Board();
        $this->winner = null;
        $this->currentPlayer = $this->players->getRandomPlayer();
    }

    /**
     * @throws DomainException
     */
    public function turn(string $playerName, int $col, int $row): void
    {
        $player = $this->players->getPlayerByName($playerName);
        if (!$player) {
            throw new DomainException("Player {$playerName} doesn't exist");
        }

        if ($player !== $this->currentPlayer) {
            throw new DomainNotAcceptableException("{$playerName} is out of the turn");
        }

        if ($this->winner) {
            return;
        }

        if ($this->board->add($row, $col, $this->currentPlayer)
            && $this->board->isWinner($this->currentPlayer)) {
            $this->winner = $this->currentPlayer;
            return;
        }

        $this->changeTurn();
    }

    public function getCurrentTurn(): Player
    {
        return $this->currentPlayer;
    }

    public function getPlayers(): array
    {
        return $this->players->getPlayers();
    }

    private function changeTurn(): void
    {
        $this->currentPlayer = $this->players->switchPlayer($this->currentPlayer);
    }

    private function checkWinner(Player $player): bool
    {
        return $this->board->isWinner($player);
    }

    public function getGameResults(): array
    {
        return $this->score->getResults();
    }

    public function getWinner(): ?Player
    {
        return $this->winner;
    }
}
