<?php

namespace App\TicTacToe\Domain\Models;

use App\TicTacToe\Domain\Exceptions\DomainException;

class Score
{
    private array $results = [];

    public function __construct(Players $players)
    {
        foreach ($players->getPlayers() as $player) {
            $this->results[$player->getName()] = 0;
        }
    }

    /**
     * @throws DomainException
     */
    public function addWinner(Player $player): void
    {
        if (!isset($this->results[$player->getName()])) {
            throw new DomainException("Wrong Player");
        }

        $this->results[$player->getName()]++;
    }

    public function getResults(): array
    {
        krsort($this->results);
        return $this->results;
    }
}
