<?php

namespace App\TicTacToe\Domain\Models;

use App\TicTacToe\Domain\Exceptions\DomainConflictException;
use App\TicTacToe\Domain\Exceptions\DomainException;

class Board
{
    private const DEFAULT_VALUE = "";
    private const DEFAULT_SIZE = 3;
    private array $data = [];

    public function __construct()
    {
        $this->reset();
    }

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @throws DomainException
     */
    public function add($row, $col, Player $player): bool
    {
        if ($this->canAdd($row, $col)) {
            $this->data[$row][$col] = $player->getName();
            return true;
        }

        return false;
    }

    public function isWinner(Player $player): bool
    {
        return $this->isHorizontalWinner($player)
            || $this->isVerticalWinner($player)
            || $this->isDiagonalWinner($player);
    }

    private function isHorizontalWinner(Player $player): bool
    {
        return array_search(array_fill(0, self::DEFAULT_SIZE, $player->getName()), $this->data) > -1;
    }

    private function isVerticalWinner(Player $player): bool
    {
        $results = [];
        for ($i = 0; $i < self::DEFAULT_SIZE; $i++) {
            $results[$i] = array_column($this->data, $i);
        }
        return array_search(array_fill(0, self::DEFAULT_SIZE, $player->getName()), $results) > -1;
    }

    private function isDiagonalWinner(Player $player): bool
    {
        $results[] = [$this->data[0][0], $this->data[1][1], $this->data[2][2]];
        $results[] = [$this->data[2][0], $this->data[1][1], $this->data[0][2]];
        return array_search(array_fill(0, self::DEFAULT_SIZE, $player->getName()), $results) > -1;
    }

    /**
     * @throws DomainException
     */
    private function canAdd($row, $col): bool
    {
        if (!in_array($row, [0, 1, 2]) || !in_array($col, [0, 1, 2])) {
            throw new DomainException("Piece coordinates outside the board");
        }

        if (isset($this->data[$row][$col]) && $this->data[$row][$col] !== self::DEFAULT_VALUE) {
            throw new DomainConflictException("Piece is already placed");
        }

        return true;
    }

    public function reset(): void
    {
        $this->data = array_fill(
            0,
            self::DEFAULT_SIZE,
            array_fill(0, self::DEFAULT_SIZE, self::DEFAULT_VALUE)
        );
    }
}
