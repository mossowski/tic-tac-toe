<?php

namespace App\TicTacToe\Domain\Models;

class Player implements \JsonSerializable
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function jsonSerialize()
    {
        return $this->name;
    }

    public function __string(): string
    {
        return $this->name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
