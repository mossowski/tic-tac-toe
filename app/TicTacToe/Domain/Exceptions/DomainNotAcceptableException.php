<?php

namespace App\TicTacToe\Domain\Exceptions;

use Illuminate\Http\Response;

class DomainNotAcceptableException extends DomainException
{
    public function render(): Response
    {
        return response(["message" => $this->getMessage()], Response::HTTP_NOT_ACCEPTABLE);
    }
}
