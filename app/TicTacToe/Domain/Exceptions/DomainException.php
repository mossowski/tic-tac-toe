<?php

namespace App\TicTacToe\Domain\Exceptions;

use Illuminate\Http\Response;

class DomainException extends \Exception
{
    public function render(): Response
    {
        return response(["message" => $this->getMessage()], Response::HTTP_BAD_REQUEST);
    }
}
