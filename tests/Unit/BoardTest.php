<?php

namespace Tests\Unit;

use App\TicTacToe\Domain\Exceptions\DomainConflictException;
use App\TicTacToe\Domain\Exceptions\DomainException;
use App\TicTacToe\Domain\Models\Board;
use App\TicTacToe\Domain\Models\Player;
use PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_can_get_default_data_successfully()
    {
        $board = new Board();
        $this->assertEquals([["", "", ""], ["", "", ""], ["", "", ""]], $board->getData());
    }

    public function test_can_add_turn_successfully()
    {
        $player = new Player("x");
        $board = new Board();
        $board->add(0, 1, $player);
        $this->assertEquals([["", "x", ""], ["", "", ""], ["", "", ""]], $board->getData());
        $this->assertFalse($board->isWinner($player));
    }

    public function test_can_horizontal_win_successfully()
    {
        $player = new Player("x");
        $board = new Board();
        $board->add(0, 0, $player);
        $board->add(0, 1, $player);
        $this->assertFalse($board->isWinner($player));
        $board->add(0, 2, $player);
        $this->assertEquals([["x", "x", "x"], ["", "", ""], ["", "", ""]], $board->getData());
        $this->assertTrue($board->isWinner($player));
    }

    public function test_can_vertical_win_successfully()
    {
        $player = new Player("x");
        $board = new Board();
        $board->add(0, 0, $player);
        $board->add(1, 0, $player);
        $this->assertFalse($board->isWinner($player));
        $board->add(2, 0, $player);
        $this->assertEquals([["x", "", ""], ["x", "", ""], ["x", "", ""]], $board->getData());
        $this->assertTrue($board->isWinner($player));
    }

    public function test_can_diagonal_win_successfully()
    {
        $player = new Player("x");
        $board = new Board();
        $board->add(0, 0, $player);
        $board->add(1, 1, $player);
        $this->assertFalse($board->isWinner($player));
        $board->add(2, 2, $player);
        $this->assertEquals([["x", "", ""], ["", "x", ""], ["", "", "x"]], $board->getData());
        $this->assertTrue($board->isWinner($player));
    }

    public function test_can_reset_successfully()
    {
        $player = new Player("x");
        $board = new Board();
        $board->add(0, 1, $player);
        $this->assertEquals([["", "x", ""], ["", "", ""], ["", "", ""]], $board->getData());
        $board->reset();
        $this->assertEquals([["", "", ""], ["", "", ""], ["", "", ""]], $board->getData());
    }

    /**
     * @throws \App\TicTacToe\Domain\Exceptions\DomainException
     */
    public function test_fail_add_turn_outside_the_board()
    {
        $this->expectException(DomainException::class);
        $player = new Player("x");
        $board = new Board();
        $board->add(0, 4, $player);
    }

    /**
     * @throws \App\TicTacToe\Domain\Exceptions\DomainException
     */
    public function test_fail_add_turn_the_same_place_twice()
    {
        $this->expectException(DomainConflictException::class);
        $player = new Player("x");
        $board = new Board();
        $board->add(0, 0, $player);
        $board->add(0, 0, $player);
    }
}
