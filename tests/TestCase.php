<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('cache:clear');
    }

    protected function switchPlayer(string $player) : string
    {
        return $player === "x" ? "o" : "x";
    }

    protected function startGameAndReturnCurrentTurn() : string
    {
        $response = $this->get("/");
        $response
            ->assertStatus(Response::HTTP_OK);
        return $response->decodeResponseJson()["currentTurn"];
    }
}
