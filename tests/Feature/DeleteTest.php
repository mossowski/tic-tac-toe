<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_can_delete_successfully_when_winner_exists()
    {
        $winner = $this->startGameAndReturnCurrentTurn();
        $secondPlayer = $this->switchPlayer($winner);
        $response = null;
        for ($i = 0; $i < 3; $i++) {
            $response = $this->post("/{$winner}", ["x" => $i, "y" => 0]);
            $response
                ->assertStatus(Response::HTTP_OK);

            if ($i == 2) {
                break;
            }
            $response = $this->post("/{$secondPlayer}", ["x" => $i, "y" => 1]);
            $response
                ->assertStatus(Response::HTTP_OK);
        }

        $response = $this->post("/restart");
        $response
            ->assertStatus(Response::HTTP_OK);

        $score = $response->json("score");
        $this->assertEquals(1, $score[$winner]);
        $this->assertEquals(0, $score[$secondPlayer]);

        $response = $this->delete("/");
        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonMissing(["score", "board", "victory"]);
    }

    public function test_can_delete_successfully_when_no_winner()
    {
        $firstPlayer = $this->startGameAndReturnCurrentTurn();
        $secondPlayer = $this->switchPlayer($firstPlayer);

        $response = $this->post("/{$firstPlayer}", ["x" => 0, "y" => 0]);
        $response
            ->assertStatus(Response::HTTP_OK);

        $response = $this->post("/{$secondPlayer}", ["x" => 1, "y" => 1]);
        $response
            ->assertStatus(Response::HTTP_OK);

        $score = $response->json("score");
        $this->assertEquals(0, $score[$firstPlayer]);
        $this->assertEquals(0, $score[$secondPlayer]);

        $response = $this->delete("/");
        $response->assertStatus(Response::HTTP_OK)
        ->assertJsonMissing(["score", "board", "victory"]);
    }

    public function test_fail_delete_when_game_not_started()
    {
        $response = $this->delete("/");
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
        $response->assertJsonPath("message", "Can't process action - game is not started");
    }
}
