<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class StartTest extends  TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_can_start_successfully()
    {
        $response = $this->get("/");
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonPath("board", [["", "", ""], ["", "", ""], ["", "", ""]])
            ->assertJsonPath("victory", "")
            ->assertJsonPath("score", ["x" => 0, "o" => 0]);
    }
}
