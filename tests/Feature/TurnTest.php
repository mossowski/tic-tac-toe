<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class TurnTest extends  TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_can_add_turn_successfully()
    {
        $currentTurn = $this->startGameAndReturnCurrentTurn();

        $response = $this->post("/{$currentTurn}", ["x" => 0, "y" => 0]);
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonPath("board", [[$currentTurn, "", ""], ["", "", ""], ["", "", ""]]);
    }

    public function test_can_add_turn_and_win_successfully()
    {
        $currentTurn = $this->startGameAndReturnCurrentTurn();
        $firstPlayer = $currentTurn;
        $response = null;
        for ($i = 0; $i < 3; $i++) {
            $response = $this->post("/{$currentTurn}", ["x" => $i, "y" => 0]);
            $response
                ->assertStatus(Response::HTTP_OK);

            $currentTurn = $this->switchPlayer($currentTurn);

            if ($i == 2) {
                break;
            }
            $response = $this->post("/{$currentTurn}", ["x" => $i, "y" => 1]);
            $response
                ->assertStatus(Response::HTTP_OK);

            $currentTurn = $this->switchPlayer($currentTurn);
        }

        $response->assertJsonPath("victory", $firstPlayer);

    }

    public function test_fail_add_turn_when_players_send_the_same_coordinates()
    {
        $currentTurn = $this->startGameAndReturnCurrentTurn();

        $response = $this->post("/{$currentTurn}", ["x" => 0, "y" => 0]);
        $response
            ->assertStatus(Response::HTTP_OK);

        $currentTurn = $this->switchPlayer($currentTurn);
        $response = $this->post("/{$currentTurn}", ["x" => 0, "y" => 0]);
        $response
            ->assertStatus(Response::HTTP_CONFLICT)
            ->assertJsonPath("message", "Piece is already placed");
    }

    public function test_fail_add_turn_when_game_not_started()
    {
        $response = $this->post("/o", ["x" => 0, "y" => 0]);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
        $response->assertJsonPath("message", "Can't process action - game is not started");
    }

    public function test_fail_add_turn_two_times_by_the_same_player()
    {
        $currentTurn = $this->startGameAndReturnCurrentTurn();

        $response = $this->post("/{$currentTurn}", ["x" => 0, "y" => 0]);
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonPath("board", [[$currentTurn, "", ""], ["", "", ""], ["", "", ""]]);

        $response = $this->post("/{$currentTurn}", ["x" => 0, "y" => 1]);
        $response
            ->assertStatus(Response::HTTP_NOT_ACCEPTABLE);
    }

    public function test_fail_add_turn_outside_the_board()
    {
        $currentTurn = $this->startGameAndReturnCurrentTurn();

        $response = $this->post("/{$currentTurn}", ["x" => 3, "y" => 0]);
        $response
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonPath("message", "Piece coordinates outside the board");
    }
}
