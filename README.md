## Tic Tac Toe with Laravel
#### by Maciej Ossowski

### About the project and implementation details

Requirements: https://assets.billy.dk/programming-assessment-task-main/src/index.html

I bootstrapped a Laravel project using Sail (Laravel Framework 8.52.0), ensure that you have installed Docker locally on your machine.
From the base project I removed most of unnecessary dependencies and docker images.
Instead of using standard MVC architecture I implemented in this project ADR (Action-Domain-Responder) approach to simplify a flow.
Core codebase is located in `app/TicTacToe` directory.

To persist a state of the game between HTTP requests I used PSR-16 common cache interface injected to the class
that decorates the main domain service class - if necessary using that pattern it would be easy to change a way how a state is persisted.
In any case of problems with a game state run a command `./vendor/bin/sail artisan cache:clear` to clean cached data.

Game is 2-player mode (players x and o are humans, send requests in their turns)

### Start the project

1. Clone the git project to the directory
2. In the directory where you copied the project please run docker to get Laravel Sail and start composer install command `docker run --rm \
   -v $(pwd):/opt \
   -w /opt \
   laravelsail/php80-composer:latest \
   composer install`
   And then `./vendor/bin/sail up -d` to build and start docker image.
3. Install composer dependencies by running a command `./vendor/bin/sail composer install`
4. open in a browser http://localhost/ or use Postman for manual tests of application features
   
### Tests
To run all Feature (functional API tests) and Unit (domain) tests use a command `./vendor/bin/sail artisan test`
