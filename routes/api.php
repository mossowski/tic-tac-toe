<?php

use App\TicTacToe\Actions\{StartAction, TurnAction, RestartAction, DeleteAction};
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("/", StartAction::class);
Route::post("/{piece}", TurnAction::class)->where("piece", "[a-z]{1}");
Route::post("/restart", RestartAction::class);
Route::delete("/", DeleteAction::class);
